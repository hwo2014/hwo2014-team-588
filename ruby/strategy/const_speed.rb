module Strategy
  class ConstSpeed
    def initialize(target)
      @target_speed = target
      @throttle = 0
      @piece_index = 0
      @piece_distance = 0
      @speed = 0
      @accel = 0
      @angle = 0
    end

    def update(pos, angle)
      idx = pos['pieceIndex']
      dist = pos['inPieceDistance']
      if idx == @piece_index
        cur_speed = dist - @piece_distance
        @accel = cur_speed - @speed
        @speed = cur_speed
      end
      angle_speed = angle.abs - @angle.abs
      if angle_speed > 0
        @throttle -= 0.50 * (angle.abs / 90)
      else
        if @speed + @accel*10 < @target_speed
          @throttle += 0.10 * (1 - (@speed / @target_speed))
        else
          @throttle -= 0.01
        end
      end
      if @throttle > 1.0
        @throttle = 1.0
      end
      if @throttle < 0.0
        @throttle = 0.0
      end
      @piece_index = idx
      @piece_distance = dist
      @angle = angle

      @throttle
    end
  end
end


require 'json'
require 'socket'
require 'optparse'
require File.join(File.dirname(__FILE__), "strategy/const_speed")

track = nil
bot_name = "Misaka_Mitoko"
bot_key = "aEG68bu1RfzaQA"
OptionParser.new do |opt|
  opt.on("-t", "--track TRACK", String, "Track name") do |t|
    track = t
  end
end.parse!

server_host = ARGV[0]
server_port = ARGV[1]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key, track)
    tcp = TCPSocket.open(server_host, server_port)
    @strategy = Strategy::ConstSpeed.new(7.0)
    play(bot_name, bot_key, track, tcp)
    play(bot_name, bot_key, track, tcp) if track.nil?
  end

  private

  def play(bot_name, bot_key, track, tcp)
    tcp.puts join_message(bot_name, bot_key, track)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          pos = msgData.first['piecePosition']
          throttle = @strategy.update(pos, msgData.first['angle'])
          print "\r%02d % 12.7f % 12.7f" % [pos['pieceIndex'], pos['inPieceDistance'], msgData.first['angle']]
          STDOUT.flush
          tcp.puts throttle_message(throttle)
        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'gameStart'
              puts 'Race started'
            when 'lapFinished'
              puts ""
              puts "Laptime: #{msgData['lapTime']}"
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
          end
          puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key, track)
    if track
      make_msg("joinRace", {
        botId: {
          name:  bot_name,
          key: bot_key
        },
        trackName: track,
        carCount: 1})
    else
      make_msg("join", {name: bot_name, key: bot_key})
    end
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end
end

NoobBot.new(server_host, server_port, bot_name, bot_key, track)
